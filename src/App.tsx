import React, { ChangeEvent, useEffect, useState } from "react";
import "./App.css";
import { Card, CardContent } from "@material-ui/core";
import { Contact } from "./components/contact";
import { ImgThumb } from "./components/img-thumb";
const colors = ["slategray", "green"];

function App(): React.ReactElement {
  const [title, setTitle] = useState("super app");
  const [contacts, setContacts] = useState([
    { name: "Joe", phone: 1234234 },
    { name: "Mike", phone: 81234234 },
    { name: "Bob", phone: 41234234 },
    { name: "Jack", phone: 34234 },
    { name: "Carlos", phone: 234234 },
  ]);
  const [images] = useState([
    "https://api.debugger.pl/assets/tomato.jpg",
    "https://api.debugger.pl/assets/pumpkin.jpg",
    "https://api.debugger.pl/assets/potatoes.jpg",
  ]);
  const [theme, setTheme] = useState(colors[0]);
  const [access, setAccess] = useState(false);
  const [currentImg, setCurrentImg] = useState();
  const [currentContact, setCurrentContact] = useState();

  const [items, setItems] = useState([]);

  useEffect(() => {
    fetchItems();
  }, []);

  async function fetchItems() {
    const resp = await fetch("https://api.debugger.pl/items");
    const { data } = await resp.json();
    setItems(data);
  }

  const changeTitle = ({
    target: { value },
  }: ChangeEvent<HTMLInputElement>) => {
    setTitle(value);
  };
  const clickHandler = (ev: any) => {
    alert("clicked " + ev.target.id);
  };
  const toggleTheme = ({ target: { value } }: any) => {
    setTheme(value);
  };
  const toggleAccess = ({ target: { checked } }: any) => {
    setAccess(checked);
  };
  return (
    <div style={{ padding: "20px", background: theme }}>
      <Card color="primary">
        <CardContent>
          <h2>display variables</h2>
          <div>{title}</div>
          <ul>
            {contacts.map((contact) => (
              <li key={contact.phone}>{contact.name}</li>
            ))}
          </ul>
        </CardContent>
      </Card>
      <br />
      <Card color="primary">
        <CardContent>
          <h2>events: click, change...</h2>
          <div>{title}</div>
          <input type="text" value={title} onChange={changeTitle} />
          <button id="my-button" onClick={clickHandler}>
            click me
          </button>
        </CardContent>
      </Card>
      <br />
      <Card color="primary">
        <CardContent>
          <h2>condition</h2>
          <label>
            <input type="checkbox" checked={access} onChange={toggleAccess} />
            toggle access
          </label>
          <div>
            your access is <strong>{access ? "ok" : "not ok"}</strong>
          </div>
          <br />
          {access &&
            colors.map((c, i) => (
              <label key={i}>
                <input
                  type="radio"
                  name="theme"
                  checked={c === theme}
                  value={c}
                  onChange={toggleTheme}
                />
                {c}
              </label>
            ))}
          <br />
        </CardContent>
      </Card>
      <br />
      <Card color="primary">
        <CardContent>
          <h2>components</h2>
          <div style={{ display: "flex", gap: 10 }}>
            <div>
              <h4>Gallery</h4>
              {images.map((url) => (
                <ImgThumb key={url} url={url} action={setCurrentImg} />
              ))}
              <br />
              {currentImg && <img src={currentImg} alt="big" />}
            </div>
            <div>
              <h4>Contacts</h4>
              {currentContact}
              {contacts.map((contact) => (
                <Contact
                  key={contact.phone}
                  data={contact}
                  action={setCurrentContact}
                />
              ))}
            </div>
          </div>
        </CardContent>
      </Card>
      <br />
      <Card>
        <CardContent>
          <h2>Komunikacja z serwerem</h2>
          {items.map((item: any) => (
            <li key={item.id}>
              {item.title}
              <img src={item.imgSrc} alt="" width="50" />
            </li>
          ))}
        </CardContent>
      </Card>
    </div>
  );
}

export default App;
