import { Button, Card, CardContent } from "@material-ui/core";
import { FunctionComponent } from "react";

interface ContactProps {
  data: any;
  action: Function;
}

export const Contact: FunctionComponent<ContactProps> = ({ data, action }) => {
  return (
    <div>
      <Card>
        <CardContent>
          {data.name}
          <Button
            variant="contained"
            onClick={() => {
              action(data.phone);
            }}
          >
            call
          </Button>
        </CardContent>
      </Card>
      <br />
    </div>
  );
};
