import { FunctionComponent } from "react";

interface ImgThumbProps {
  url: string;
  action: Function;
}

export const ImgThumb: FunctionComponent<ImgThumbProps> = ({ url, action }) => {
  return (
    <img
      src={url}
      width="50"
      alt=""
      onClick={() => {
        action(url);
      }}
    />
  );
};
